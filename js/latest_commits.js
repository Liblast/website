const commits_container = document.querySelector('.commits tbody')
const showMoreBtn = document.querySelector('.show-more')
const limit = 3
let loading = false
let page = 1

async function getLatestCommits() {
  loading = true
	// Get the 3 latest commits
  const resp = await fetch(`https://codeberg.org/api/v1/repos/Liblast/Liblast/commits?limit=${limit}&page=${page}`);
  if (resp.status == 200) {
    let html = '';

    resp.json().then( commits => {
      commits.forEach(commit => {
        const date = new Date(commit.created)
        const created = timeAgo(date)
        const author = {
          username    : (!!commit.author) ? commit.author.username : commit.commit.author.name,
          avatar      : (!!commit.author) ? commit.author.avatar_url : 'https://codeberg.org/assets/img/avatar_default.png',
          profile_url : (!!commit.author) ? `https://codeberg.org/${commit.author.login}` : '',
        }

        html += `
          <tr>
            <td class="author">
              <div>
                <img src="${author.avatar}" />
        `
        if (author.profile_url) {
          html += `
            <a class="author-link" href="${author.profile_url}">
              ${author.username}
            <a/></div>
          `
        } else html += `<span>${author.username}</span></div>`
        
        html+=`
            </td>
            <td class="commit-msg">
              ${nl2br(commit.commit.message)}
            </td>
            <td class="commit-date">
              ${created}
            </td>
          </tr>
        `
      })

      commits_container.innerHTML += html
      loading = false
    })
  }
}

getLatestCommits()

showMoreBtn.addEventListener('click', event => {
  event.preventDefault()
  if (page == 8 || loading) return

  page++;
  getLatestCommits()
})
