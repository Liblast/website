let mediaContainer = document.querySelector('.gallery')

function activateGallery(){
  let images = mediaContainer.querySelectorAll('img')
  let intervalCount = 0
  setInterval(() => {
    images.forEach(img => {
      img.classList.remove('active')
    });
    images[intervalCount % images.length].classList.add('active')
    intervalCount ++
  }, 3000); 
}
activateGallery()

// Some utilities

function timeAgo(timestamp) {
  let ago;

  const diff = (new Date().getTime() - timestamp.getTime()) / 1000;
  const minutes = Math.floor(diff / 60);
  const hours = Math.floor(minutes / 60);
  const days = Math.floor(hours / 24);
  const months = Math.floor(days / 30);
  const years = Math.floor(months / 12);
  const rtf = new Intl.RelativeTimeFormat('en', { numeric: "auto" });

  if (years > 0) {
    ago = rtf.format(0 - years, "year");
  } else if (months > 0) {
    ago = rtf.format(0 - months, "month");
  } else if (days > 0) {
    ago = rtf.format(0 - days, "day");
  } else if (hours > 0) {
    ago = rtf.format(0 - hours, "hour");
  } else if (minutes > 0) {
    ago = rtf.format(0 - minutes, "minute");
  } else {
    ago = rtf.format(0 - diff, "second");
  }
  return ago
}

/**
 * This function is same as PHP's nl2br() with default parameters.
 * 
 * https://gist.github.com/yidas/41cc9272d3dff50f3c9560fb05e7255e
 *
 * @param {string} str Input text
 * @param {boolean} replaceMode Use replace instead of insert
 * @param {boolean} isXhtml Use XHTML 
 * @return {string} Filtered text
 */
function nl2br (str, replaceMode, isXhtml) {

  var breakTag = (isXhtml) ? '<br />' : '<br>';
  var replaceStr = (replaceMode) ? '$1'+ breakTag : '$1'+ breakTag +'$2';
  return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, replaceStr);
}

// For show YouTube advisement before show the iframe