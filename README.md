# Website for https://libla.st

Static HTML / CSS / JS website for the libre multiplayer FPS game Liblast.

![Liblast logo](images/liblast_og.png)

## License

The content of this repository is under [GNU AGPL](https://www.gnu.org/licenses/agpl-3.0.en.html), except the graphics and the font files, respectively under [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) and [SIL OFL](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL).